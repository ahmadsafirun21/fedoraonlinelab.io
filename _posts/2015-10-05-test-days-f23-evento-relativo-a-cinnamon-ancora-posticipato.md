---
categories:
- staff news
layout: ultime_news
title: 'Test Days F23: evento relativo a Cinnamon ancora posticipato!'
created: 1444071214
---
<p><img alt="" src="http://i816.photobucket.com/albums/zz84/bankotsu109/test-days-250px.png" style="width: 190px; height: 119px;" /></p>
<p>Come <a href="http://www.fedoraonline.it/content/test-days-f23-cinnamon">comunicato in precedenza</a>, nella data di ieri (04 Ottobre 2015) era prevista la giornata relativa al testing di Cinnamon.</p>
<p>Sembra per&ograve; che la nuova spin di Fedora dovr&agrave; ancora attendere: solitamente infatti, per attirare pi&ugrave; utenti possibili, i Test Days non vengono organizzati nel finesettimana. La scelta della prima domenica di Ottobre quindi, sembra essere stata frutto di un banale errore di battitura.</p>
<p>Secondo le prime indiscrezioni, l&#39;evento dovrebbe finalmente svolgersi <strong>Gioved&igrave; 08 Ottobre 2015</strong>. Si attende ora l&#39;annuncio ufficiale da parte del team Quality Assurance del Fedora Project.</p>
