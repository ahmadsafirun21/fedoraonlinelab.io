---
categories:
- staff news
layout: ultime_news
title: Pizzata Fedora e Release Party F17
created: 1335475284
---
<p>
	Penstavate voi che ci eravamo dimenticati, eh? <img alt="smiley" height="20" src="http://www.fedoraonline.it/sites/all/modules/ckeditor/ckeditor/plugins/smiley/images/regular_smile.gif" title="smiley" width="20" /><br />
	Invece voglio annunciarvi ufficialmente <strong>la terza pizzata Fedora</strong>, che stavolta presenta alcune novit&agrave; rispetto alle prime due edizioni:</p>
<p>
	1. http://doc.fedoraonline.it/Prima_pizzata_Fedora_Online-La_vera_storia 2. http://doc.fedoraonline.it/Pizzata_2010 Inanzitutto vogliamo festeggiare il raggiungimento dei 7.000 utenti, ma anche la nuova versione di Fol, il primo numero di Folio ecc ecc ecc<br />
	Ma non finisce qui:<br />
	Abbiamo aspettato a lungo perch&egrave; prima c&#39;era bisogno di sistemare altre cose, ma anche perch&egrave; volevamo che fosse un appuntamento per tutti gli utenti Fedora, e non solo per gli utenti Fol. Rappresentiamo la comunit&agrave; italiana di Fedora e sarebbe bello poter conoscere anche qualche altro personaggio del mondo Fedora/RedHat, speriamo di riuscire anche in questo.<br />
	Inoltre faremo coincidere la pizzata con un [b]Release Party[/b], per cui festeggeremo anche la nuovissima Fedora 17, nome in codice Beefy Miracle, ma non per questo andremo a mangiarci hamburger (preferisco una bella pizza).<br />
	Ultima novit&agrave;: cambia (finalmente direte) il luogo!<br />
	Ci vedremo, e spero davvero che sarete in tanti:<br />
	<br />
	<strong>Sabato 9 giugno 2012 a ROMA</strong><br />
	<br />
	a cena! L&#39;orario ve lo diremo prossimamente, perch&egrave; vorremmo gi&agrave; prenotare per un numero ipotetico di partecipanti.<br />
	La Pizzeria sar&agrave; in zona Piazza di Spagna e per chi, come me, pernotter&agrave; successivamente, vi daremo, come di consueto, anche qualche suggerimento su alberghi economici (mediamente zona Termini). Come per il FudCon sentitevi liberi di organizzarvi tramite questa discussione per pernottare in una doppia con un altro utente, e quindi di risparmiare ulteriormente.<br />
	<br />
	Vi chiedo di farmi sapere via mail oppure qui in questa discussione se ci siete, e di attivarvi per la data del 9 giugno. Per chi arriva da fuori segnalo inoltre Italo, il nuovo treno ad alta velocit&agrave;, che &egrave; appena partito e offre tariffe economiche di lancio per contrastare l&#39;AV di Trenitalia.<br />
	<br />
	Vi aspetto (aspettiamo) numerosi!<br />
	Ciao Robyduck</p>
<p>
	Link discussione sul forum: http://forum.fedoraonline.it/viewtopic.php?id=18058</p>
