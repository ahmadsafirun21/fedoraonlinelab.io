---
categories:
- staff news
layout: ultime_news
title: 'Ora siamo anche su Google+ : venite a trovarci!'
created: 1356823918
---
<div class="postmsg">
	<p>
		Vi annuncio con piacere che da oggi siamo presenti anche su <a href="https://plus.google.com/u/0/communities/107468696249989523000">Google+</a>, sfruttando la nuova funzione delle comunit&agrave; e inserendoci al canale italiano creato da poco. Vorrei ringraziarea questo punto il creatore del canale Stefano per averci accolto (dandoci anche la possibilit&agrave; di gestire il canale insieme a lui) e creando cos&igrave; un unico canale italiano nel quale possiamo parlare di Fedora.<br />
		Ho appena aggiunto l&#39;icona di Google+ in fondo alle pagine di Fol per averlo sempre a disposizione.</p>
	<p>
		Vi aspettiamo, siamo gi&agrave; un bel po&#39; di utenti, unitevi a noi <img alt="smile" src="http://forum.fedoraonline.it/img/smilies/smile.png" /></p>
	<p>
		Ciao</p>
</div>
<p>
	&nbsp;</p>
