---
categories:
- staff news
layout: ultime_news
title: Elezioni Council/FESCo/FAmSCo/Env & Stacks
created: 1448790432
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><span style="font-family:verdana,geneva,sans-serif;">Il periodo di candidatura per le prossime elezioni Fedora, si &egrave; concluso.</span><span style="font-family:verdana,geneva,sans-serif;"> Di seguito, una breve descrizione relativa agli organi in oggetto:</span></p>
<p class="rtejustify">&nbsp;</p>
<pre class="rtejustify">
<span style="font-family:verdana,geneva,sans-serif;"><span class="cmdline">- <strong>Council</strong>: massimo organo decisionale della comunit&agrave;.
</span></span></pre>
<pre class="rtejustify">
<span style="font-family:verdana,geneva,sans-serif;"><span class="cmdline">- <strong>FESCo</strong>: si occupa delle nuove caratteristiche, pacchetti, e relative componenti tecniche.</span></span></pre>
<p class="rtejustify"><span style="font-family:verdana,geneva,sans-serif;"><span class="cmdline">- <strong>FAmSCo</strong>: organo che coordina e rappresenta gli ambassador</span>.</span></p>
<p class="rtejustify"><span style="font-family:verdana,geneva,sans-serif;">- <strong>Env &amp; Stacks</strong>: gruppo operativo che ricerca, sviluppa o migliora i metodi di sviluppo, testing, pacchettizzazione e rilascio di software per la comunit&agrave;.</span></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><span style="font-family:verdana,geneva,sans-serif;">Gli Italiani in lizza sono i seguenti:</span></p>
<p class="rtejustify"><span style="font-family:verdana,geneva,sans-serif;">- <em>Robert Mayr (Robyduck)</em>, per Council</span></p>
<p class="rtejustify"><span style="font-family:verdana,geneva,sans-serif;">- <em>Germano Massullo (Caterpillar)</em> per FESCo</span></p>
<p class="rtejustify"><span style="font-family:verdana,geneva,sans-serif;">- <em>Gabriele Trombini (mailga)</em>, per FAmSCo</span></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><span style="font-family:verdana,geneva,sans-serif;">Le votazioni sono riservate ai possessori di un account registrato sul sistema &quot;Fedora Account System&quot;, che abbiano inoltre firmato il &quot;<span class="st">Contributor License Agreement</span>&quot;. Le interviste ai vari candidati saranno pubblicate sul Fedora Community Blog. Il periodo in cui si potr&agrave; esprimere la propria preferenza, parte con il giorno 08 Dicembre 2015 e si conclude il giorno 14.</span></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><span style="font-family:verdana,geneva,sans-serif;"><strong>Link utili</strong>:</span></p>
<p class="rtejustify"><span style="font-family:verdana,geneva,sans-serif;">- <strong>Lista completa candidati</strong>: <a href="https://lists.fedoraproject.org/archives/list/devel-announce%40lists.fedoraproject.org/thread/7X67DJFOZSIU3W4JZ7NILNDAVJWNNUIG/">https://lists.fedoraproject.org/archives/list/devel-announce%40lists.fedoraproject.org/thread/7X67DJFOZSIU3W4JZ7NILNDAVJWNNUIG/</a></span></p>
<p class="rtejustify"><span style="font-family:verdana,geneva,sans-serif;">- <strong>Fedora Community Blog</strong>: <a href="https://communityblog.fedoraproject.org">https://communityblog.fedoraproject.org</a></span></p>
