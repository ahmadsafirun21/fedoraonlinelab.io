---
categories:
- staff news
layout: ultime_news
title: Fedora 18 Release Party Napoli
created: 1362058205
---
<p><strong>Marted&igrave; 5 marzo dalle 10:30 alle 12:30 a Napoli </strong>si svolger&agrave; un Release Party di Spherical Cow, ovvero Fedora 18.</p>
<p>Vi aspettiamo tutti all&#39;Universi&agrave; di Napoli &quot;Federico II&quot;, Via Nuova Agnano - 80125 - Napoli.</p>
<p>Programma:</p>
<ul>
	<li>
		Presentazione del Progetto Fedora e della comunit&agrave; italiana</li>
	<li>
		Novit&agrave; di Fedora 18</li>
	<li>
		Install Party</li>
	<li>
		USB Station</li>
	<li>
		Distribuzione DVD e gadget</li>
</ul>
<p>Durante il Party ci si potr&agrave; connettere anche su #fedora-it, server freenode, per porre domande o semplicemente&nbsp;per stare in contatto con la comunit&agrave; e il Party. Inoltre &egrave; probabile che, per chi fosse impossibilitato a essere presente,&nbsp;sar&agrave; possibile seguire l&#39;evento in streaming; in quel caso seguiranno ulteriori comunicazioni.</p>
<p><strong>Non mancate!</strong></p>
<p><a href="http://fedoraproject.org/wiki/Release_Party_F18_Naples"><strong>http://fedoraproject.org/wiki/Release_Party_F18_Naples</strong></a></p>
<p><img alt="" src="https://lh6.googleusercontent.com/-XuoLqaM-dpU/US82AUQUpjI/AAAAAAAAC_o/O4H61_FWXwc/s645/2013-02-28" style="width: 456px; height: 645px" /></p>
