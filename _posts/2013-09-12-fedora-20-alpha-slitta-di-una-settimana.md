---
categories:
- staff news
layout: ultime_news
title: Fedora 20 Alpha slitta di una settimana
created: 1379023030
---
<p>Oggi &egrave; stato deciso che la prima versione test (Alpha) di Heisenbug (il nome di Fedora 20) arriver&agrave; una settimana pi&ugrave; tardi di quanto previsto. E&#39; per via di un bug di anaconda ma soprattutto delle immagini i386 Cloud che questa decisione si &egrave; resa necessaria.</p>
<p>Potete leggere tutto il log del meeting qui:</p>
<p><a href="http://meetbot.fedoraproject.org/fedora-meeting-2/2013-09-12/f20_alpha_gono-go_meeting.2013-09-12-17.03.html">http://meetbot.fedoraproject.org/fedora-meeting-2/2013-09-12/f20_alpha_gono-go_meeting.2013-09-12-17.03.html</a></p>
<p>La Release Schedule aggiornata &egrave; disponibile qui:</p>
<p><a href="https://fedoraproject.org/wiki/Releases/20/Schedule">https://fedoraproject.org/wiki/Releases/20/Schedule</a></p>
