---
categories:
- staff news
layout: ultime_news
title: Risultati elezioni gennaio 2017 - mailga e robyduck vincono
created: 1484642971
---
<p>Con grande piacere vi annuncio che i due candidati italiani sono stati eletti per tutti i seggi per cui si sono proposti. Un grazie a chi ha fatto possibile questo, ora sta a loro (noi) metterci al lavoro per il prossimo anno.</p>
<p>Ecco il dettaglio delle votazioni:</p>
<p><span style="font-size:14px;"><strong>Council: (1 seggio)</strong></span></p>
<table id="results" style="border-spacing: 0px; color: rgb(102, 102, 102); font-family: &quot;Liberation Sans&quot;, &quot;Lucida Grande&quot;, &quot;Luxi Sans&quot;, &quot;Bitstream Vera Sans&quot;, helvetica, verdana, arial, sans-serif; font-size: small;">
	<tbody>
		<tr>
			<th style="vertical-align: text-top; line-height: 19px;">
				Candidate</th>
			<th style="vertical-align: text-top; line-height: 19px;" title="Number of votes received">
				Votes</th>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Robert Mayr (robyduck)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Robyduck" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				743</td>
		</tr>
		<tr class="firstout row_even" style="background-color: rgb(240, 240, 240);">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px; border-top: 2px solid red !important;">
				Justin W. Flory (jflory7)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Jflory7" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px; border-top: 2px solid red !important;">
				738</td>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Giannis Konstantinidis (giannisk)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Giannisk" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				466</td>
		</tr>
		<tr class="row_even" style="background-color: rgb(240, 240, 240);">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Charles Profitt (cprofitt)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Cprofitt" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				413</td>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Itamar Reis Peixoto (itamarjp/itamarjp)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Itamarjp" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				393</td>
		</tr>
	</tbody>
</table>
<p><span style="font-size:14px;"><strong>FAmSCo: (7 seggi, rielezione totale)</strong></span></p>
<table id="results" style="border-spacing: 0px; color: rgb(102, 102, 102); font-family: &quot;Liberation Sans&quot;, &quot;Lucida Grande&quot;, &quot;Luxi Sans&quot;, &quot;Bitstream Vera Sans&quot;, helvetica, verdana, arial, sans-serif; font-size: small;">
	<tbody>
		<tr>
			<th style="vertical-align: text-top; line-height: 19px;">
				Candidate</th>
			<th style="vertical-align: text-top; line-height: 19px;" title="Number of votes received">
				Votes</th>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Robert Mayr (robyduck)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Robyduck" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				1623</td>
		</tr>
		<tr class="row_even" style="background-color: rgb(240, 240, 240);">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Jona Azizaj (jonatoni)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Jonatoni" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				1576</td>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Gabriele Trombini (mailga)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Mailga" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				1274</td>
		</tr>
		<tr class="row_even" style="background-color: rgb(240, 240, 240);">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Giannis Konstantinidis (giannisk)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Giannisk" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				1168</td>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Itamar Reis Peixoto (itamarjp)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Itamarjp" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				1110</td>
		</tr>
		<tr class="row_even" style="background-color: rgb(240, 240, 240);">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Frederico Lima (fredlima)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Fredlima" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				1010</td>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Sylvia Sanchez (Kohane / lailah)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Lailah" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				964</td>
		</tr>
		<tr class="firstout row_even" style="background-color: rgb(240, 240, 240);">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px; border-top: 2px solid red !important;">
				Sirko Kemter (gnokii)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Gnokii" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px; border-top: 2px solid red !important;">
				944</td>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Zacharias Mitzelos (mitzie)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Mitzie" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				920</td>
		</tr>
		<tr class="row_even" style="background-color: rgb(240, 240, 240);">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Marcel Ribeiro Dantas (mribeirodantas)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Mribeirodantas" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				862</td>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Daniel Lara (danniel)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Danniel" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				856</td>
		</tr>
		<tr class="row_even" style="background-color: rgb(240, 240, 240);">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Lucas Landim (landim)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Landim" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				735</td>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Tulio Macedo (_Teseu_ / teseu)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Teseu" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				731</td>
		</tr>
	</tbody>
</table>
<p><span style="font-size:14px;"><strong><span style="font-family: arial, sans-serif;">FESCo: (5 seggi)</span></strong></span></p>
<table id="results" style="border-spacing: 0px; color: rgb(102, 102, 102); font-family: &quot;Liberation Sans&quot;, &quot;Lucida Grande&quot;, &quot;Luxi Sans&quot;, &quot;Bitstream Vera Sans&quot;, helvetica, verdana, arial, sans-serif; font-size: small;">
	<tbody>
		<tr>
			<th style="vertical-align: text-top; line-height: 19px;">
				Candidate</th>
			<th style="vertical-align: text-top; line-height: 19px;" title="Number of votes received">
				Votes</th>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Kevin Fenzi (nirik / kevin)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Nirik" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				1401</td>
		</tr>
		<tr class="row_even" style="background-color: rgb(240, 240, 240);">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Adam Miller (maxamillion / maxamillion)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Maxamillion" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				1075</td>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Jared Smith (jsmith / jsmith)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Jsmith" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				988</td>
		</tr>
		<tr class="row_even" style="background-color: rgb(240, 240, 240);">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Justin Forbes (jforbes / jforbes)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:jforbes" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				735</td>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Kalev Lember (kalev / kalev)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Kalev" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				691</td>
		</tr>
		<tr class="firstout row_even" style="background-color: rgb(240, 240, 240);">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px; border-top: 2px solid red !important;">
				Itamar Reis Peixoto (itamarjp / itamarjp)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Itamarjp" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px; border-top: 2px solid red !important;">
				558</td>
		</tr>
		<tr class="row_odd">
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				Frederico Lima (fredlima / fredlima)&nbsp;<small><a href="https://fedoraproject.org/wiki/User:Fredlima" style="color: rgb(0, 102, 204); text-decoration: none;">[info]</a></small></td>
			<td style="padding-left: 15px; padding-right: 15px; line-height: 19px;">
				539</td>
		</tr>
	</tbody>
</table>
<div>
	&nbsp;</div>
<div>
	&nbsp;</div>
<div>
	&nbsp;</div>
