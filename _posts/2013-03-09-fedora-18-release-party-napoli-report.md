---
categories:
- staff news
layout: ultime_news
title: Fedora 18 Release Party Napoli - Report
created: 1362868186
---
<p><img alt="" src="http://www.nalug.net/system/files/imagecache/full/foto/Fedora18_Release_Party@Unina/IMG_8938.JPG" style="width: 400px; height: 267px;" /></p>
<p><span style="font-family: arial, sans-serif; font-size: 13px;">5 marzo 2013, il NaLUG in collaborazione con la Comunit&agrave; Italiana di Fedora presentano il Fedora Release Party; location dell&#39;evento la facolt&agrave; di Ingegneria &quot;Federico II&quot; di Napoli.</span> <span style="font-family: arial, sans-serif; font-size: 13px;">Per promuovere e discutere della nuova versione di Fedora &quot;Spherical Cow&quot; la giornata &egrave; stata divisa in due momenti:</span></p>
<p><span style="font-family: arial, sans-serif; font-size: 13px;">1) Il primo si &egrave; svolto nell&#39;atrio della facolt&agrave;, posizione strategica per riuscire a catturare l&#39;attenzione di tutti i ragazzi e promuovere gli eventi della giornata attraverso la consegna del programma e invitando i curiosi a provare e installare Fedora distribuendone i DVD.</span><br style="font-family: arial, sans-serif; font-size: 13px;" />
	<span style="font-family: arial, sans-serif; font-size: 13px;">Tra i 3000 studenti che ogni giorno aflluiscono all&#39;universit&agrave;, siamo riusciti ad attirare, tramite questo spazio, l&#39;attenzione di circa 1/3 di questi, che incuriositi hanno chiesto informazioni riguardanti tematiche da &quot;cos&#39;&egrave; Fedora?&quot; a problematiche tecniche pi&ugrave; specifiche.&nbsp;</span><span style="font-family: arial, sans-serif; font-size: 13px;"> Per invogliare i neofiti a provare Fedora gli sono stati consegnati i DVD multidesktop con la possibilit&agrave; di un &quot;help desk&quot;.</span></p>
<p><span style="font-family: arial, sans-serif; font-size: 13px;">2) Il secondo si &egrave; tenuto presso un&#39;aula della sede Universitaria dove sono state svolte le seguenti attivit&agrave; in programma:</span></p>
<ul>
	<li>
		<span style="font-family: arial, sans-serif; font-size: 13px;">Apertura del contest, che consisteva nel risolvere degli errori in un semplice script SH (che &egrave; possibile trovare su&nbsp;</span><a href="http://www.nalug.net/contest/FRPcontest" style="color: rgb(17, 85, 204); font-family: arial, sans-serif; font-size: 13px;" target="_blank">http://www.nalug.net/contest/<wbr />FRPcontest</a><span style="font-family: arial, sans-serif; font-size: 13px;">), per i vincitori sono stati messi in palio le tazze e cappelli con il logo fedora.</span></li>
	<li>
		<span style="font-family: arial, sans-serif; font-size: 13px;">I Talk: a cura di Giuseppe Del Vecchio (aka Virus della comunity italiana di Fedoraonline.it), che con la sua grande competenze e capacit&agrave; comunicativa &egrave; riuscito a coinvolgere i partecipanti sulle seguenti tematiche:</span></li>
</ul>
<div class="im" style="color: rgb(80, 0, 80); font-family: arial, sans-serif; font-size: 13px;">
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; - Cosa &egrave; Fedora<br />
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; - La storia delle versioni di Fedora<br />
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; - Il Fedora Project<br />
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; - La comunity italiana&nbsp;<a href="http://fedoraonline.it/" style="color: rgb(17, 85, 204);" target="_blank">fedoraonline.it</a><br />
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; - Installazione/aggiornamento a Fedora 18<br />
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; - Intro a SELinux e systemd</div>
<p><span style="font-family: arial, sans-serif; font-size: 13px;">alla fine dell&#39;intervento durato circa un ora e mezza si &egrave; aperto un momento di confronto tra i partecipanti e il relatore per chiarimenti ed approfondimenti.</span></p>
<ul>
	<li>
		<span style="font-family: arial, sans-serif; font-size: 13px;">Il coffee break: per rendere ancora pi&ugrave; &quot;appetitosa&quot; la giornata e per incentivare l&#39;incontro di pensieri e persone, sono stati offerti hai partecipanti cornetti e bevande.</span></li>
	<li>
		<span style="font-family: arial, sans-serif; font-size: 13px;">Install party: dopo la presentazione &egrave; stato fornito supporto a tutti coloro che volevano installare Fedora 18 o aggiornare dalla 17.</span></li>
</ul>
<p><span style="font-family: arial, sans-serif; font-size: 13px;">E&#39; stata una fantastica giornata, molte persone hanno conosciuto Fedora, altri hanno potuto approfondire le loro conoscenze su Fedora, sul Project e sulla comunity, quindi posso dire, non senza un pizzico di orgoglio, che l&#39;obbiettivo &egrave; stato pienamente raggiunto. Colgo l&#39;occasione per ringraziare, nuovamente Giuseppe Del Vecchio ed i ragazzi del NaLUG per la disponibilit&agrave; concessami, nella speranza che questo sia stato il primo di una lunga collaborazione.</span></p>
