---
categories:
- staff news
layout: ultime_news
title: Ci siamo! Fedora 18 verrà rilasciata il 15 gennaio
created: 1357768674
---
<p>Pochi minuti fa la Release Candidate 3 di Fedora 18 &egrave; stata definita &quot;GOLD&quot;, che vuole dire che &egrave; stata ritenuta pronta per il rilascio.</p>
<p>Domani ci sar&agrave; un altro <u>call for RC 4</u>, ma se per qualche motivo non dovesse passare anche lei come &quot;Gold&quot;, <strong>Fedora 18 verr&agrave; rilasciata in ogni caso il 15 gennaio</strong> (con l&#39;immagine della RC 3).<br />
	<strong>Evviva</strong>!</p>
