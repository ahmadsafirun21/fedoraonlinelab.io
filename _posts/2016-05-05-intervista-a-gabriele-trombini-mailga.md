---
categories:
- staff news
layout: ultime_news
title: Intervista a Gabriele Trombini (mailga)
created: 1462478342
---
<p>
	<style type="text/css">
p { margin-bottom: 0.25cm; line-height: 120%; }	</style>
</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%">Riportiamo di seguito la traduzione italiana dell&#39;articolo &quot;<a href="https://fedoramagazine.org/gabriele-trombini-fedora/">Gabriele Trombini: How do you Fedora?</a>&quot; di Charles Profitt, pubblicato su Fedora Magazine il giorno 29 Aprile 2016.</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%">&nbsp;</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%"><img alt="mailga" src="https://fedoramagazine.org/wp-content/uploads/2016/04/gabri_interview.jpg" style="width: 200px; height: 84px;" /></p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%">&nbsp;</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%"><strong><em>Chi &egrave; Gabriele Trombini?</em></strong></p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%"><a href="http://forum.fedoraonline.it/profile.php?id=901">Gabriele Trombini</a> &egrave; a capo di un&#39;impresa che opera nel campo degli impianti di riscaldamento e climatizzazione. Le sue 10/12 ore lavorative giornaliere, consistono nel dialogare con fornitori e clienti per stringere accordi commerciali. Internet e globalizzazione hanno reso questo compito pi&ugrave; difficoltoso rispetto al passato, a causa della grande <span style="background: transparent">mole</span> di concorrenza. Gabriele sfrutta la sua ampia esperienza nel <span style="background: transparent">servizio alla clientela </span><span style="background: transparent">e</span> in ambito promozionale<span style="background: transparent">,</span> contribuendo ai team <a href="https://fedoraproject.org/wiki/Marketing">Marketing</a>, <a href="https://fedoraproject.org/wiki/Fedora_Join_SIG">Join</a> e <a href="https://fedoraproject.org/wiki/CommOps">CommOps</a> del Fedora Project.</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%">Trombini inizia ad usare Linux quando i desktop erano pi&ugrave; grandi degli schermi CRT (o, in altre parole, molto tempo fa). La lista degli eroi d&#39;infanzia include Spider-Man, Donald Duck, David Bowie e i Led Zeppelin. Grande fan di John Belushi, deve ancora trovare una persona a cui non piaccia il cibo italiano.</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%">&nbsp;</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%"><em><strong>Comunit&agrave; Fedora</strong></em></p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%">Gabriele &egrave; un <a href="https://fedoraproject.org/wiki/Ambassadors">Fedora Ambassador</a> impiegato sia a livello locale che in campo internazionale. In particolare, &egrave; attratto dall&#39;atmosfera calorosa e gioviale che si respira contribuendo al Project. Chiunque ha l&#39;opportunit&agrave; di condividere opinioni e informazioni all&#39;interno di un ambiente amichevole e <span style="background: transparent">votato</span> alla collaborazione. Trombini sottolinea che, il rispetto per gli altri e la volont&agrave; di rinnovare, sono necessari per mantenere forte e coesa la comunit&agrave; di Fedora. &ldquo;Proviamo qualcosa di nuovo: se non genera i risultati sperati, dobbiamo essere pronti per cambiare la direzione intrapresa&rdquo;, ripete.</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%">Quando gli &egrave; stato chiesto di descrivere il tipo di lavoro che svolge in ambito Fedora, Gabriele ha risposto con la parola latina &ldquo;factotum&rdquo;, lasciando all&#39;intervistatore il compito di comprenderne la definizione. Quest&#39;ultimo, ha imparato che, il vocabolo in questione, equivale al dire che Mailga svolge diversi compiti e ricopre diversi ruoli.</p>
<p align="center" style="margin-bottom: 0cm; line-height: 100%"><em>&ldquo;Factotum: una persona la cui occupazione richiede lo svolgimento di diversi tipi di lavoro&quot;</em></p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%">Gettando uno sguardo al futuro del Progetto Fedora, &egrave; opinione di Trombini, che il lavoro da svolgere debba essere allineato, giorno dopo giorno, passo dopo passo, agli obiettivi prefissati dall&#39;intera <span style="background: transparent">comunit&agrave;</span>. Per questo, ai nuovi contributori, egli suggerisce cautela: &egrave; sbagliato condividere subito troppe idee. Il consiglio &egrave; quindi quello di lavorare gradualmente, preferibilmente a quegli obbiettivi del Project che pi&ugrave; si allineano alle attitudini personali del volontario.</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%">&nbsp;</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%"><em><strong>Quale hardware?</strong></em></p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%">Trombini ha diverse macchine con Fedora: un Raspberry Pi, un notebook HP e un HP Proliant. Il primo della serie, viene usato per Git, Web, Email e condivisione di documenti. Il laptop &egrave; il suo computer principale, mentre HP Proliant rappresenta il server della sua azienda.</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%">&nbsp;</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%"><em><strong>Quale software?</strong></em></p>
<p align="justify" style="margin-bottom: 0cm; line-height: 100%">Gabriele ha lavorato a diverse versioni di Fedora 23, mentre contribuiva agli annunci di rilascio. Per quanto riguarda la sua impresa, utilizza LibreOffice, Scribus, Inkscape e Gimp. I suoi compiti legati al mondo del Fedora Project, sono svolti grazie sopratutto alla combinazione di strumenti come Geany e Git.</p>
