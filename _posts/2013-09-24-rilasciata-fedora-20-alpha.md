---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 20 Alpha
created: 1380032254
---
<p>E&#39; stata appena rilasciata Fedora 20 Alpha, nome in codice Heisenbug. Le novit&agrave; sono davvero tantissime e per la prima volta, insieme alle versioni Desktop &quot;solite&quot;, sono state pubblicate le immagini Cloud e ARM, che d&#39;orain poi rientreranno nei release blocker in quanto architetture primarie.</p>
<p>Per chi volesse provarla linkiamo qui la pagina dei Common Bugs, importantissima:</p>
<p><a href="https://fedoraproject.org/wiki/Common_F20_bugs">https://fedoraproject.org/wiki/Common_F20_bugs</a></p>
<p>L&#39;annuncio completo, in cui si fa riferimento anche alle novit&agrave; introdotte si pu&ograve; leggere al seguente indirizzo:</p>
<p><a href="https://lists.fedoraproject.org/pipermail/announce/2013-September/003182.html">https://lists.fedoraproject.org/pipermail/announce/2013-September/003182.html</a></p>
