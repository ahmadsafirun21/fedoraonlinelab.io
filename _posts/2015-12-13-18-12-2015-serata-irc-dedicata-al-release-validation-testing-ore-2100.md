---
categories:
- staff news
layout: ultime_news
title: 18/12/2015 - Serata IRC dedicata al Release Validation Testing (ore 21:00)
created: 1450002628
---
<p class="rtejustify"><img alt="" src="https://fedoraproject.org/w/uploads/6/65/QA.png" style="width: 468px; height: 60px;" /></p>
<p>&nbsp;</p>
<p class="rtejustify">Il giorno <strong>venerd&igrave; 18/12/2015 (ore 21:00 italiane)</strong>, si terr&agrave; una serata IRC dedicata al testing.</p>
<p class="rtejustify">L&#39;oggetto dell&#39;evento, sar&agrave; una simulazione relativa al Release Validation Testing, attivit&agrave; del team Quality Assurance del Fedora Project.</p>
<p class="rtejustify">Tale processo, raccoglie l&#39;insieme di criteri, passaggi e controlli, che vengono applicati nei confronti dei futuri rilasci del sistema operativo.</p>
<p class="rtejustify">Trattandosi di simulazione, l&#39;unico requisito per partecipare all&#39;incontro, &egrave; il possesso di un&#39;installazione fisica o in macchina virtuale di una versione supportata di Fedora (22, 23 o Rawhide).</p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><strong>Link utili</strong>:</p>
<p class="rtejustify"><strong>- Release validation test plan</strong>: <a href="https://fedoraproject.org/wiki/QA:Release_validation_test_plan">https://fedoraproject.org/wiki/QA:Release_validation_test_plan</a></p>
<p class="rtejustify"><strong>- Regole</strong>: <a href="http://doc.fedoraonline.it/Meeting_IRC#Regole">http://doc.fedoraonline.it/Meeting_IRC#Regole</a></p>
