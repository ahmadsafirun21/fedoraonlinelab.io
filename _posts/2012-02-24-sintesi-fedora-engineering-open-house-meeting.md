---
categories:
- staff news
layout: ultime_news
title: Sintesi Fedora Engineering "Open House" Meeting
created: 1330073703
---
<p>
	Ieri sera, 23 febbraio, si &egrave; tenuto il meeting &quot;Open House&quot; della squadra di Fedora Engineering, al quale era stata invitata tutta la comunit&agrave;, per presentare e parlare delle novit&agrave; previste nei prossimi 12 mesi.<br />
	Vi vogliamo riassumere quanto &egrave; stato discusso, proprio per condividere l&#39;esperienza di ieri sera, alla quale erano presenti, oltre al sottoscritto, antowen, wardialer e verso la fine del meeting virus. Inoltre non poteva mancare l&#39;amico Andrea Veri per quanto riguarda la presenza italiana, oltre a M&agrave;ir&igrave;n Duffy, Robyn Bergeron, i &quot;colleghi&quot; francesi llaumgui e pingou. Nel corso della durata del meeting, durato circa un&#39;ora e mezza, gli utenti erano tra i 160 e 170.<br />
	<br />
	Veniamo al sodo: dopo l&#39;apertura dell&#39;incontro alle 19:07 e la presentazione del Fedora Engineering Team si &egrave; subito partiti con i primi temi, andando in ordine:</p>
<p>
	<strong>* Abilitazione AMQP</strong>: Leader Ralph Bean<br />
	AMQP &egrave; un layer aperto per la gestione dei flussi di messaggi in un sistema enterprise. Esso mira all&#39;ottimizzazione di una serie di processi ed &egrave; molto interessante sapere che il team attualmente si sta indirizzando verso 0MQ (zeromq), forse il pi&ugrave; snello e veloce. Alla domanda specifica di Robyn il team risponde che &egrave; previsto di modificare l&#39;intero processo di bugzilla proprio utilizzando 0MQ. Vedremo come la cosa si evolve, di sicuro bugzilla necessit&agrave; di una gestione pi&ugrave; interattiva e veloce.<br />
	<br />
	<strong>* Bodhi 2.0</strong>: Leader Luke Macken<br />
	Bodhi &egrave; molto importante, in quanto &egrave; l&#39;applicativo web utilizzato per aggiornare le release di Fedora. La riscrittura di Bodhi (attualmente alla versione 1.0) mira a facilitare il processo e l&#39;utilizzo.<br />
	<br />
	<strong>* Eucalyptus Sandbox</strong>: Leader Seth Vidal<br />
	<br />
	<strong>* Miglioramento applicativo Mailing List</strong>: Leader Tom Callaway<br />
	L&#39;obiettivo &egrave; quello di realizzare una piattaforma simil-forum che si interfacci con le ML. Questo deve soddisfare gli utenti che amano i forum, ma deve anche accontentare chi invece tende di pi&ugrave; ad usare le Mailing List.<br />
	Si &egrave; parlato molto della terza versione di mailman, che &egrave; alla base del progetto, ma anche della preoccupazione di mantenere tutte le liste attuali, in modo da dare la massima continuit&agrave;. Il progetto &egrave; comunque, nonostante la creazione di alcuni mockup, nella prima fase di sviluppo.<br />
	<br />
	<strong>* Manutenzione di applicazioni esistenti</strong>: Leader Toshio Kuratomi<br />
	Si &egrave; detto che in questi anni si &egrave; lavorato molto su nuove applicazioni per il servizio di Fedora, ma che molti sviluppatori hanno cambiato aria e le applicazioni sono rimasti &quot;abbandonate&quot; a se stesse. L&#39;obiettivo quindi &egrave; quello di rilasciare, per gli applicativi pi&ugrave; utili, nuovi rilasci e fixare i bug dove &egrave; possibile. Inoltre il team vuole trovare nuovi maintainer delle app. Kuratomi far&agrave; direttamente da tutor, per&ograve; pretende chiaramente che i volontari siano realmente interessati e presenti.<br />
	<br />
	<strong>Altri progetti minori</strong>:<br />
	<strong>a)</strong> Altre apps come games<br />
	http://community.dev.fedoraproject.org/tagger &egrave; il primo &quot;game&quot; Fedora<br />
	<strong>b)</strong> Statistics++: Leader Ian Weller<br />
	Lavorando sul messangin bus l&#39;obiettivo &egrave; quello di creare un applicativo che permetta di estrarre dei report a qualsiasi utente abilitato in qualsiasi momento. Si sta valutando l&#39;aspetto legale e della privacy.<br />
	<strong>c)</strong> Analisi statica: Leader David Malcolm<br />
	Il tool &quot;gcc-with-cpychecker&quot; dovrebbe evitare gli errori di gcc quando si compilano moduli python; questo sar&agrave; portato probabilmente gi&agrave; in F17. In un futuro non immediato si vuole arrivare a rimpiazzare l&#39;attuale gcc con una versione patchata per Fedora.<br />
	<strong>d)</strong> Pacchetti Fedora e Tagger<br />
	Oltre alla chiusura di bugs noti si vuole inserire un API di ricerca in Package Kit che dia un risultato diverso dall&#39;attuale repodata.<br />
	<strong>e)</strong> Miglioramento della ricerca di Fedora: Leader Ricky Elrod<br />
	Si sta cercando di migliorare la ricerca, ma anche di fornire nel wiki una ricerca semplificata (l&#39;attuale viene definita orribile), con pagine di index e una maggiore usabilit&agrave;.<br />
	<br />
	Il meeting si chiude alla 20:41.<br />
	<br />
	Per chi volesse, il log completo &egrave; disponibile qui:</p><br />
<a href="http://meetbot.fedoraproject.org/fedora-meeting/2012-02-23/fedora-meeting.2012-02-23-18.05.log.html">http://meetbot.fedoraproject.org/fedora-meeting/2012-02-23/fedora-meeting.2012-02-23-18.05.log.html</a>
