---
categories:
- staff news
layout: ultime_news
title: 'Fedora 27 Beta è GO: verrà rilasciata il 03 Ottobre 2017!'
created: 1506681738
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p>Nel corso del terzo Go/No-Go Meeting, relativo a Fedora 27 Beta, i team QA, Release Engineering e Development hanno deciso di dare il via libera al rilascio.<br />
	<br />
	Fedora 27 Beta sar&agrave; quindi resa pubblicamente disponibile il giorno <strong>03 Ottobre 2017</strong>.<br />
	<br />
	Il log della riunione &egrave; consultabile <a href="https://meetbot.fedoraproject.org/fedora-meeting-1/2017-09-28/f27-beta-go-no-go-meeting-3rd.2017-09-28-17.00.log.html">qui</a>.<br />
	&nbsp;</p>
<p>Allo stato attuale (salvo eventuali ulteriori slittamenti), le date previste per i rilasci successivi, sono le seguenti:<br />
	<br />
	- 07/11/2017 Rilascio di Fedora 27 Final</p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify">&nbsp;</p>
