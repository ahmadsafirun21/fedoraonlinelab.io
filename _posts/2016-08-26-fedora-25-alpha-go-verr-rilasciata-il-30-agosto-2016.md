---
categories:
- staff news
layout: ultime_news
title: 'Fedora 25 Alpha è GO: verrà rilasciata il 30 Agosto 2016!'
created: 1472209151
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p>Nel corso del secondo Go/No-Go Meeting, relativo a Fedora 25 Alpha, i team QA, Release Engineering e Development hanno deciso di dare il via libera al rilascio.<br />
	<br />
	Fedora 25 Alpha sar&agrave; quindi resa pubblicamente disponibile il giorno <strong>30 Agosto 2016</strong>.<br />
	<br />
	Il log della riunione &egrave; il seguente: <a href="https://meetbot.fedoraproject.org/fedora-meeting-2/2016-08-25/f25-alpha-go_no_go-meeting.2016-08-25-17.04.log.html">https://meetbot.fedoraproject.org/fedora-meeting-2/2016-08-25/f25-alpha-go_no_go-meeting.2016-08-25-17.04.log.html </a><br />
	&nbsp;</p>
<p>Allo stato attuale (salvo eventuali ulteriori slittamenti), le date previste per i rilasci successivi, sono le seguenti:<br />
	<br />
	- 11/10/2016 Rilascio di Fedora 25 Beta<br />
	- 15/11/2016 Rilascio di Fedora 25 Final</p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify">&nbsp;</p>
