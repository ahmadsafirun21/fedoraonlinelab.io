---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 26 Beta
created: 1497446462
---
<p><img alt="" src="https://cdn.fedoramagazine.org/wp-content/uploads/2017/05/f26-beta-945x400.jpg" style="height: 199px; width: 470px;" /></p>
<p>&nbsp;</p>
<p>In data 13 giugno &egrave; stata rilasciata Fedora 26 Beta:</p>
<p>Link per scaricare Fedora 26 Beta Workstation: <a href="https://getfedora.org/it/workstation/prerelease/">https://getfedora.org/it/workstation/prerelease/</a></p>
<p>L&#39;edizione Workstation di Fedora 26 Beta include tra le altre novit&agrave;: GNOME 3.24, la nuova funzione Night Light (che permette di cambiare la temperatura del colore in base all&#39;orario della giornata), e l&#39;ultima versione di LibreOffice.</p>
<p>&nbsp;</p>
