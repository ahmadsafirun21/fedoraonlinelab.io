---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 18 - Spherical Cow
created: 1358248640
---
<p>E&#39; stata appena rilasciata ufficialmente Fedora 18, scaricabile da qui:<br />
	<a href="http://get.fedoraproject.org/">http://get.fedoraproject.org/</a></p>
<p>Come di consueto consigliamo la lettura di bug noti, che, si spera, vengano risolti man mano in queste prime settimane di vita:<br />
	<a href="http://fedoraproject.org/wiki/Common_F18_bugs">http://fedoraproject.org/wiki/Common_F18_bugs</a></p>
<p><strong>IMPORTANTE: </strong>Il nuovissimo Anaconda pu&ograve; in qualche caso portare a qualche bug, alcuni noti, altri meno. Idem per il Secure Boot.</p>
<p>Caratteristiche principali:</p>
<ul>
	<li>
		Gnome 3.6</li>
	<li>
		KDE 4.9</li>
	<li>
		Xfce 4.10</li>
	<li>
		Mate</li>
	<li>
		Nuova InstallerUI (Anaconda)</li>
	<li>
		Secure Boot</li>
	<li>
		FedUp, nuovo tool di aggiornamento</li>
	<li>
		firewalld sostituisce iptables come firewall di deafult</li>
	<li>
		DNF (package maintainer sperimentale)</li>
	<li>
		Apache 2.4</li>
	<li>
		Samba 4</li>
	<li>
		Python 3.3</li>
	<li>
		Terminali a 256 colori</li>
</ul>
<p>ecc ecc, ulteriori informazioni sono disponibili qui:<br />
	<a href="http://fedoraproject.org/wiki/Releases/18/FeatureList">http://fedoraproject.org/wiki/Releases/18/FeatureList</a></p>
<p>Le note di rilascio ufficiali sono disponibili qui: <a href="http://docs.fedoraproject.org/en-US/Fedora/18/html/Release_Notes/">http://docs.fedoraproject.org/en-US/Fedora/18/html/Release_Notes/</a></p>
<p>E&#39; diponibile inoltre la nostra guida per aggiornare il sistema con il nuovo tool di aggiornamento: <a href="http://doc.fedoraonline.it/Aggiornamento_Fedora_con_FedUp">http://doc.fedoraonline.it/Aggiornamento_Fedora_con_FedUp</a></p>
