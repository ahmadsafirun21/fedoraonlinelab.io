---
categories:
- staff news
layout: ultime_news
title: Flock 2017
created: 1504031784
---
<p><img alt="" src="https://fedoraproject.org/w/uploads/4/4e/Flock-2017_sm.png" style="width: 250px; height: 275px;" /></p>
<p>Iniziato oggi 29 agosto il <strong>Fedora Flock 2017</strong> a Hyannis (Massachusetts, U.S.A.), che terminer&agrave; il giorno 1 settembre.</p>
<p>All&#39;evento interverranno tre ambassador italiani, ben conosciuti sul forum di Fedoraonline:</p>
<p>Robert Mayr (Robyduck) - Gabriele Trombini (Mailga) - Andrea Masala (Veon)</p>
<p>&nbsp;</p>
<p>Link ufficiale: <a href="https://www.flocktofedora.org/">https://www.flocktofedora.org/</a></p>
<p>Link al programma: <a href="https://flock2017.sched.com/">https://flock2017.sched.com/</a></p>
