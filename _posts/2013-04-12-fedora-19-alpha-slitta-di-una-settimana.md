---
categories:
- staff news
layout: ultime_news
title: Fedora 19 Alpha slitta di una settimana
created: 1365748420
---
<p>Per via di alcuni bug seri su UEFI la Alpha di Fedora 19 verr&agrave; posticipata di una settimana. La nuova data di rilascio quindi &egrave; il <strong>23 aprile.</strong></p>
<p>Per chi si aspettava un block per colpa di Anaconda questo non vuol dire che non c&#39;&egrave; nulla sull&#39;installer. Vedremo al rilascio della beta se il partizionamento personalizzato sar&agrave; ok, attualmente non lo &egrave;,&nbsp;ma siccome stiamo parlando della Alpha non rientra tra i motivi validi per bloccare il rilascio.</p>
<p>Tutta la discussione del meeting tenutosi ieri sera &egrave; reperibile qui:</p>
<p><a href="http://meetbot.fedoraproject.org/fedora-meeting-1/2013-04-11/f19_alpha_gono-go_meeting.2013-04-11-17.00.log.html">http://meetbot.fedoraproject.org/fedora-meeting-1/2013-04-11/f19_alpha_gono-go_meeting.2013-04-11-17.00.log.html</a></p>
