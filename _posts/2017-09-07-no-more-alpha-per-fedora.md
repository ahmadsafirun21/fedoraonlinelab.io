---
categories:
- staff news
layout: ultime_news
title: '"No more Alpha" per Fedora'
created: 1504788854
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p>&nbsp;</p>
<p><span style="font-size:12px;"><span style="font-family: verdana,geneva,sans-serif;"><span style="color: rgb(0, 0, 0); font-style: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none;">A partire dal processo che terminer&agrave; con la pubblicazione della versione 27, Fedora non rilascer&agrave; pi&ugrave; milestone di tipo Alpha.</span></span></span></p>
<p><br style="color: rgb(0, 0, 0); font-family: Cantarell; font-size: 14.666666984558105px; font-style: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0.4); -webkit-text-stroke-width: 0px;" />
	<span style="font-size:12px;"><span style="font-family: verdana,geneva,sans-serif;"><span style="color: rgb(0, 0, 0); font-style: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none;">Con il perfezionamento di nuovi sistemi di testing automatizzati (come openQA), la versione di sviluppo Rawhide beneficier&agrave; di controlli mirati per far s&igrave; che i criteri di qualit&agrave; di una Alpha siano sempre rispettati.</span><span style="color: rgb(0, 0, 0); font-style: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none;"> Questo cambiamento (decisamente innovativo) promette sostanzialmente&nbsp; di offrire la possibilit&agrave; di risparmiare tempo e risorse, senza per&ograve; sacrificare quelli che sono gli standard di Fedora.</span></span></span></p>
<p><span style="font-size:12px;"><span style="font-family: verdana,geneva,sans-serif;">Link utili:</span></span></p>
<ul>
	<li>
		<span style="font-size:12px;"><span style="font-family: verdana,geneva,sans-serif;">Sommario cambiamento: <a href="https://fedoraproject.org/wiki/Changes/NoMoreAlpha">https://fedoraproject.org/wiki/Changes/NoMoreAlpha</a></span></span></li>
</ul>
