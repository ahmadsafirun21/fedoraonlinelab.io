---
categories:
- staff news
layout: ultime_news
title: Fedora 19 si chiamerà Schrödinger's Cat
created: 1353051438
---
<p>
	Le votazioni per scegliere il nome di Fedora 19 si sono appena concluse e sono ora pubbliche.</p>
<p>
	Il nome di Fedora 19 sar&agrave;: <strong>Schr&ouml;dinger&#39;s Cat</strong></p>
<p>
	Di seguito riportiamo anche gli altri nomi proposti e i voti ottenuti. Il numero massimo teorico possibile di voti era di 3.128 voti.</p>
<p>
	<font face="Courier New">Voti &nbsp;::&nbsp; Nome<br />
	--------------------------------------------<br />
	1876&nbsp; ::&nbsp; Schr&ouml;dinger&#39;s Cat<br />
	1620&nbsp; ::&nbsp; Higgs Boson<br />
	1012&nbsp; ::&nbsp; Tiddalik<br />
	960&nbsp;&nbsp; ::&nbsp; Loch Ness Monster<br />
	907&nbsp;&nbsp; ::&nbsp; Newtonian Dynamics<br />
	892&nbsp;&nbsp; ::&nbsp; Martian Blueberries<br />
	722&nbsp;&nbsp; ::&nbsp; Parabolic Potassium<br />
	595&nbsp;&nbsp; ::&nbsp; Cubical Calf</font><br />
	<br />
	Fonte: Announce Mailing List</p>
