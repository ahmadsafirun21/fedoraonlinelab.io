---
categories:
- staff news
layout: ultime_news
title: L'uscita di Fedora 26 Alpha è stata rimandata al 04/04/2017
created: 1490439828
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p>Il rilascio di Fedora 26 Alpha &egrave; stato rimandato al <strong>04 aprile 2017</strong>.</p>
<p>Sono infatti sorti ulteriori blug bloccanti, che hanno causato lo slittamento di un&#39;altra settimana.</p>
<p>Come di consueto, il &quot;via libera&quot; &egrave; subordinato alla decisione del &quot;Go/no-go Meeting&quot; di Gioved&igrave; 30 marzo.</p>
<p>&nbsp;</p>
<p><strong>Link utili</strong>:</p>
<ul>
	<li>
		<a href="https://fedoraproject.org/wiki/Go_No_Go_Meeting">Informazioni relative ai Go/No-go Meeting</a></li>
	<li>
		<a href="https://fedoraproject.org/wiki/Fedora_26_Alpha_Release_Criteria">Criteri di rilascio per Fedora 26 Alpha</a></li>
	<li>
		<a href="http://qa.fedoraproject.org/blockerbugs/milestone/26/alpha/buglist">Lista aggiornata dei bug bloccanti</a></li>
	<li>
		<a href="https://fedoraproject.org/wiki/Releases/26/Schedule">Fedora 26 release schedule</a></li>
</ul>
