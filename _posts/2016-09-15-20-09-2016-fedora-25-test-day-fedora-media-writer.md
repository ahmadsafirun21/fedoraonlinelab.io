---
categories:
- staff news
layout: ultime_news
title: '20/09/2016: Fedora 25 Test Day (Fedora Media Writer)'
created: 1473966008
---
<p><img alt="" src="http://i816.photobucket.com/albums/zz84/bankotsu109/test-days-250px.png" style="width: 190px; height: 119px;" /></p>
<p>&nbsp;</p>
<p><strong>Introduzione</strong></p>
<p>Nella giornata di Marted&igrave; 20 Settembre 2016, &egrave; prevista una giornata di testing.<br />
	<br />
	L&#39;obiettivo sar&agrave; quello di sperimentare le funzionalit&agrave; del nuovo Fedora Media Writer - in passato conosciuto come LiveUSB Creator - all&#39;interno di diversi sistemi operativi e architetture. Il nuovo strumento sar&agrave; fornito come opzione primaria per il download di Fedora 25 Workstation, con lo scopo di facilitare la prova e l&#39;installazione di Fedora.<br />
	<br />
	La pagina relativa all&#39;evento &egrave; la seguente: <a href="https://fedoraproject.org/wiki/Test_Day:2016-09-20_Fedora_Media_Writer">https://fedoraproject.org/wiki/Test_Day:2016-09-20_Fedora_Media_Writer</a><br />
	I risultati devono essere inseriti separatamente: <a href="http://testdays.fedorainfracloud.org/events/9">http://testdays.fedorainfracloud.org/events/9</a><br />
	<br />
	&nbsp;<br />
	<strong>Preparazione del sistema</strong>:<br />
	<br />
	- Occorre avere una chiavetta USB da almeno 4 GB e un computer sul quale poter provare la Live.<br />
	<br />
	- Sono necessari i privilegi amministrativi all&#39;interno del sistema operativo usato per creare il supporto.<br />
	<br />
	- La versione pi&ugrave; recente di Fedora Media Writer (si veda la sezione seguente).<br />
	<br />
	<br />
	<strong>Come testare</strong>:<br />
	<br />
	&Egrave; sufficiente installare il tool richiesto.<br />
	<br />
	- All&#39;interno di Fedora:<br />
	<strong><em># dnf --enablerepo=updates-testing --refresh --best install mediawriter</em></strong></p>
<p>- All&#39;interno di Windows: estrarre ed avviare l&#39;eseguibile, reperibile <a href="https://mbriza.fedorapeople.org/liveusb-creator.zip">qui</a><br />
	<br />
	<strong>&nbsp;&nbsp; &nbsp;<br />
	Casi da testare</strong>:<br />
	<br />
	La pagina dei risultati - descritta nel paragrafo &quot;Introduzione&quot; - contiene i link che descrivono i passaggi da seguire.<br />
	&nbsp;<br />
	<br />
	<strong>IRC</strong>:<br />
	<br />
	Il canale internazionale ufficiale per l&#39;evento &egrave; il consueto #fedora-test-day.</p>
