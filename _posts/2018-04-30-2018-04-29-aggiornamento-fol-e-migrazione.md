---
categories:
- staff news
layout: ultime_news
title: 'Aggiornamento FOL e migrazione'
created: 1525087321
---
<p><img alt="" src="https://www.fedoraonline.it/sites/default/files/logofol_0_0.jpg" style="height:98px; width:720px" /></p>

<p>&Egrave; in corso un aggiornamento di Fedora Online, con annessa migrazione.</p>

<p>L&#39;amministratore Frafra ha aperto un <a href="https://forum.fedoraonline.it/viewtopic.php?pid=245665#p245665">topic</a> sulla questione, specificando che sar&agrave; possibile riscontrare problemi temporanei, nell&#39;utilizzo del portale.</p>

<p>Ci scusiamo in anticipo per eventuali inconvenienti.</p>
