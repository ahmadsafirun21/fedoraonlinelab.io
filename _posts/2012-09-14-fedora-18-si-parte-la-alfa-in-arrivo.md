---
categories:
- staff news
layout: ultime_news
title: 'Fedora 18: si parte! La Alfa è in arrivo'
created: 1347606061
---
<p>
	Ieri sera, in un meeting durato parecchio tempo, &egrave; stato deciso, nonostante il perseguirsi di alcuni bug su UEFI, di dichiarare la Alpha Release di Fedora 18 GOLD, ovvero pronta per essere rilasciata. Con questa decisione la versione Alpha vedr&agrave; la luce <u><strong>marted&igrave; 18 settembre 2012</strong></u>.</p>
<p>
	Si ringraziano tutti gli sviluppatori per il grosso impegno, anche perch&egrave; c&#39;&egrave; ancora molto lavoro da fare per ottenere una &quot;Spherical Cow&quot; come si deve. I dettagli del meeting sono consultabili qui:<br />
	Minutes: <a href="http://meetbot.fedoraproject.org/fedora-meeting-1/2012-09-13/f18_alpha_gono-go_meeting.2012-09-13-19.00.html" target="_blank">http://meetbot.fedoraproject.<wbr />org/fedora-meeting-1/2012-09-<wbr />13/f18_alpha_gono-go_meeting.<wbr />2012-09-13-19.00.html</a><br />
	Log: <a href="http://meetbot.fedoraproject.org/fedora-meeting-1/2012-09-13/f18_alpha_gono-go_meeting.2012-09-13-19.00.log.html" target="_blank">http://meetbot.fedoraproject.<wbr />org/fedora-meeting-1/2012-09-<wbr />13/f18_alpha_gono-go_meeting.<wbr />2012-09-13-19.00.log.html</a></p>
<p>
	<strong>IMPORTANTE</strong>: leggere attentamente la documentazione e i bug noti, di cui si parla nel Go/No-Go meeting.</p>
<p>
	Visto lo stato di sviluppo, &egrave; importante partecipare ai test per aiutare gli sviuppatori a risolvere pi&ugrave; velocemente i bug presenti. Pu&ograve; essere d&#39;aiuto la pagina creata (o meglio riesumata) su Google+ per avere sempre gli ultimi aggiornamenti:</p>
<p>
	<a href="https://plus.google.com/112917221531140868607/">https://plus.google.com/112917221531140868607/</a></p>
