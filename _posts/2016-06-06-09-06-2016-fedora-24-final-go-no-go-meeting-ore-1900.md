---
categories:
- staff news
layout: ultime_news
title: '09/06/2016 Fedora 24 Final Go/No-Go Meeting (ore 19:00)'
created: 1465241893
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p>Gioved&igrave; 09 Aprile 2016, alle 19:00 (ore italiane), si terr&agrave; il Go/No-Go Meeting Internazionale. Il luogo &egrave; il canale &quot;#fedora-meeting-1&quot;, sul server IRC Freenode.<br />
	<br />
	Prima di ogni rilascio ufficiale, i team Development, QA e Release Engineering, si incontrano per stabilire se determinati criteri qualitativi sono stati rispettati. La riunione &egrave;, ovviamente aperta a tutti.<br />
	<br />
	La disponibilit&agrave; di una RC (Release Canditate) e un certo livello di copertura garantita da test, sono prerequisiti essenziali. Se il tutto non sar&agrave; pronto per la data indicata, si discuter&agrave; ulteriormente a riguardo dei bug bloccanti (blocker bugs) e si programmeranno le azioni future.<br />
	&nbsp;<br />
	<br />
	Attualmente, la data prevista per il rilascio, &egrave; la seguente:<br />
	<br />
	- 14/06/2016 Rilascio di Fedora 24 Final<br />
	<br />
	&nbsp;<br />
	Link utili:<br />
	- Evento su calendario Fedocal: <a href="https://apps.fedoraproject.org/calendar/meeting/4107/">https://apps.fedoraproject.org/calendar/meeting/4107/</a><br />
	- Informazioni relative ai Go/No-go Meeting <a href="https://fedoraproject.org/wiki/Go_No_Go_Meeting">https://fedoraproject.org/wiki/Go_No_Go_Meeting</a><br />
	- Criteri di rilascio per Fedora 24 Final: <a href="https://fedoraproject.org/wiki/Fedora_24_Final_Release_Criteria">https://fedoraproject.org/wiki/Fedora_24_Final_Release_Criteria</a><br />
	- Lista aggiornata dei bug bloccanti: <a href="http://qa.fedoraproject.org/blockerbugs/milestone/24/final/buglist">http://qa.fedoraproject.org/blockerbugs/milestone/24/final/buglist</a></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify">&nbsp;</p>
