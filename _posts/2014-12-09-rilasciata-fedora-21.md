---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 21!
created: 1418157270
---
<p>A un anno di distanza e dopo un lavoro enorme, oggi &egrave; stata rilasciata Fedora 21, conosciuta anche come Fedora.next. Le novit&agrave; sono tantissime, non solo dal punto di vista degli aggiornamenti, ma come ho scritto nel mio&nbsp;<a href="http://robyduck.fedoraonline.it/?p=66">blogpost</a>&nbsp;soprattutto dal punto di vista delle immagini prodotte.</p>
<p>Oltre al mio post vi segnalo&nbsp;<a href="http://fedoramagazine.org/announcing-fedora-21/">l&#39;annuncio ufficiale su fedoramagazine</a>&nbsp;dove potete rendervi conto del lavoro svolto.</p>
<p><img alt="" src="http://www.fedoraonline.it/sites/all/themes/newfol20/images/Fedora-21-livemedia-workstation-64-thumb-2.png" style="width: 400px; height: 400px;" /></p>
<p>Ultimo ma non ultimo, abbiamo anche un nuovo sito web ufficiale. Buon divertimento!</p>
