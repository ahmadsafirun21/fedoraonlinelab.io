---
categories:
- staff news
layout: ultime_news
title: 'Elezioni Fedora: mailga e robyduck candidati. Votazioni aperte!'
created: 1484048574
---
<p>E&#39; di nuovo quel periodo, ovvero la settimana delle votazioni. Infatti da oggi sono aperte le elezioni per il Fedora Engineering Steering Committee (FESCo), il Fedora Ambassadors Steering Committee (FAmSCo) e il Council. Per votare per il Council basta avere un<a href="https://admin.fedoraproject.org/accounts"> FAS account</a>&nbsp;e aver firmato la CLA , mentre per il FESCo e per il FAmSCo bisogna far parte di almeno un gruppo, oltre al CLA (CLA+1).&nbsp;</p>
<p>Si vota per i seguenti seggi:</p>
<ul>
	<li>
		FESCo: 5 seggi vacanti</li>
	<li>
		FAmSCo: 7 seggi vacanti, viene rieletto completamente (candidati <strong>mailga</strong> e <strong>robyduck</strong>)</li>
	<li>
		Council: 1 seggio (candidato <strong>robyduck </strong>per la rielezione)</li>
</ul>
<p>Il sistema di voto &eacute; a punteggio, per cui si assegnano i voti maggiori ai propri preferiti, mentre si lascia a zero chi non si vuole votare. E&acute;possibile dare anche dei voti intermedi, giusto per dire &quot;mi piace ma non mi convince del tutto&quot;. Le elezioni sono un momento molto importante per il Fedora Project, perch&eacute; determinano in parte dove Fedora andr&aacute; nei prossimi mesi/anni. Tutti sono invitati a partecipare, chi non &eacute; iscritto al FAS pu&oacute; farlo in pochi semplici passi.</p>
<p>Per saperne di pi&uacute; sulle votazioni date un&#39;occhiata a <a href="https://fedoraproject.org/wiki/Elections">questa pagina wiki</a>, mentre l&#39;applicativo per le votazioni vere e proprie &eacute; reperibile<a href="https://admin.fedoraproject.org/voting/"> qui</a>.</p>
<p>Inutile dire, che se si vuole dare voce alla comunit&aacute; italiana sono da preferire i candidati locali. Le interviste ai candidati saranno disponibili in questi giorni sul<a href="https://communityblog.fedoraproject.org/"> blog del Fedora Project</a>.</p>
