---
categories:
- staff news
layout: ultime_news
title: Fedora 26 Alpha slitta di una settimana
created: 1488474719
---
<p>Rimandato al <strong>21 marzo 2017</strong> il rilascio di Fedora 26 Alpha.</p>
<p>&nbsp;</p>
<p>Link utili:</p>
<p>https://fedoraproject.org/wiki/Releases/26/Schedule</p>
<p>https://fedoraproject.org/wiki/Fedora_26_Alpha_Release_Criteria</p>
