---
categories:
- staff news
layout: ultime_news
title: '05/01/2016 - Meeting IRC Italiano Fedora (ore 22:00 #fedora-it)'
created: 1451995944
---
<p><img alt="" src="https://scontent-cdg2-1.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/12369249_1039992026045829_6478223874346465556_n.png?oh=0d8accca51238b3d85ae8ad36b4b448b&amp;oe=570F35A6" style="width: 400px; height: 184px;" /></p>
<p>&nbsp;</p>
<p>Ricordiamo che <strong>stasera, marted&igrave; 05 Gennaio 2016</strong>, &egrave; previsto il consueto <strong>meeting IRC, alle ore 22:00</strong> (ora locale).</p>
<p>Le coordinate per il collegamento sono le seguenti:</p>
<p><strong>Server</strong>: freenode<br />
	<strong>Canale</strong>: #fedora-it</p>
<p>&nbsp;</p>
<p>Gli argomenti sono elencati negli appositi appunti:</p>
<p><a href="http://doc.fedoraonline.it/Meeting_IRC#Ordine_del_giorno_della_prossima_riunione">http://doc.fedoraonline.it/Meeting_IRC#Ordine_del_giorno_della_prossima_riunione</a></p>
<p>Ulteriori informazioni, sono disponibili nella pagina dedicata:</p>
<p><a href="http://doc.fedoraonline.it/Meeting_IRC">http://doc.fedoraonline.it/Meeting_IRC</a></p>
<p>&Egrave; raccomandata una lettura alle regole generali:</p>
<p><a href="http://doc.fedoraonline.it/Meeting_IRC#Regole">http://doc.fedoraonline.it/Meeting_IRC#Regole</a></p>
