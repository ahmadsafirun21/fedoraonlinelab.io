---
categories:
- staff news
layout: ultime_news
title: '13/10/2015 - Stasera Meeting Italiano Fedora (ore 22:00 #fedora-it)'
created: 1444715641
---
<p><img alt="" src="https://scontent-mxp1-1.xx.fbcdn.net/hphotos-xpl1/v/t1.0-9/12065641_1009084105803288_2332435489623145255_n.png?oh=f6775e9c662de3ad4985f4f972d499d2&amp;oe=56883527" style="width: 400px; height: 184px;" /></p>
<p>&nbsp;</p>
<p>Ricordiamo che <strong>oggi, marted&igrave; 13 Ottobre 2015</strong>, &egrave; previsto il consueto <strong>meeting IRC, alle ore 22:00</strong> CET (ora locale).</p>
<p>Le coordinate per il collegamento sono le seguenti:</p>
<p><strong>Server</strong>: freenode<br />
	<strong>Canale</strong>: #fedora-it</p>
<p>Ulteriori informazioni e l&#39;agenda sono disponibili nella pagina dedicata:<br />
	<a href="http://doc.fedoraonline.it/Meeting_IRC">http://doc.fedoraonline.it/Meeting_IRC</a></p>
<p>&Egrave; raccomandata una lettura alle regole generali:<br />
	<a href="http://doc.fedoraonline.it/Meeting_IRC#Regole">http://doc.fedoraonline.it/Meeting_IRC#Regole</a></p>
