---
categories:
- staff news
layout: ultime_news
title: Matthew Miller sarà il nuovo Fedora Project Leader
created: 1401802093
---
<p>Dopo le intenzioni di lasciare l&#39;attuale incarico, espresse dall&#39;attuale FPL Robyn Bergeron, poco fa la stessa Robyn ha annunciato il suo successore. Il nuovo Fedora Project Leader sar&agrave; Matthew Miller, conosciuto da tutti tra gli addetti ai lavori come il Mr. Cloud, perch&egrave; si occupa principalmente di questo aspetto. Con l&#39;arrivo di fedora.next &egrave; riuscito a tenere la community abbastanza unita anche su questioni molto delicate, e personalmente trovo che sia veramente adatto a questo nuovo ruolo.</p>
<p>La comunit&agrave; italiana Fedora vuole augurare buona fortuna a Matthew e che le sue decisioni che dovr&agrave; prendere siano quelle pi&ugrave; giuste per Fedora.</p>
<p>Potete leggere tutto l&#39;annuncio (in inglese) negli arvchivi della Mailing List:&nbsp;<a href="https://lists.fedoraproject.org/pipermail/advisory-board/2014-June/012585.html">https://lists.fedoraproject.org/pipermail/advisory-board/2014-June/012585.html</a></p>
